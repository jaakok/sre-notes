# Part 2
## Chapter 3 - Embracing Risk

- Extreme reliability comes at a cost, 100% reliability ends up with added dev costs and little to no returns in actual use
- Users can't distinguish between a service that is 99% available and 99.99% available
- SRE is about risk management and deciding what is an appropriate amount of risk and downtime for a given service
- Google Apps for Work (Enterprise package) has considerably higher availability targets than more high-velocity products like early Youtube
    - Therefore availability must be matched with the needs of customers and users and assessed with developer needs in mind
- 

## Chapter 4

Go back to Chapter 4 at later time https://sre.google/sre-book/service-level-objectives/

## Chapter 5

Go back to chapter 5 at a later time https://sre.google/sre-book/eliminating-toil/


## Monitoring

Below this header are chapters describing monitoring. Used as part 3 of the presentation

https://sre.google/sre-book/monitoring-distributed-systems/
https://sre.google/sre-book/practical-alerting/
https://sre.google/sre-book/managing-incidents/
https://sre.google/sre-book/tracking-outages/

## Chapter 6 

- Terminology
    - Monitoring: Act of collecting and processing real-time quantitative data about a system; query counts, error counts, processing times etc.
    - White-box monitoring: Monitoring the internals of a system, logs, interfaces, handlers
    - Black-box monitoring: Testing UI behaviour as a user would typically see it
    - Dashboard: UI that displays core service metrics
    - Alert: Notification intended to be read by a human and that is pushed to a system like a ticket queue
    - Root cause: The fundamental cause behind an error that, when removed, will instill confidence that a particular bug won't appear again
    - Node and machine: Single instance of a running kernel in either physical devices, VMs, or containers
- Why monitor?
    - Analyze long-term trends: How quickly is user-base growing, how are the internal systems like databases keeping up
    - Comparing over time or experiment groups: How much adding a new node impacted the service? How much faster is using Framework A over Framework B?
    - Alerting: Something is broken and needs to be fixed now!
- Monitoring and alerting enables a system to tell developers when it's broken or is about to break, if the system is unable to get back up by itself we want to alert a human person to take action
- Golden monitoring metrics
    - Latency, Traffic, Errors, Saturation
- General philosophy of alerting
    - Every time a pager goes off I should be able to react with urgency. This can only be done a few times a day before it becomes too fatiguing
    - Every page should be actionable
    - Every page should require some intelligence to solve, if it's a robotic fix it can be fixed by a robot
    - Pages should be unique and about a novel problem
- Monitoring enables service owners to make decisions about the impact of changes to the service, apply rational decision making into incident response and validate their reason for existence; measure service's alignment with business goals

