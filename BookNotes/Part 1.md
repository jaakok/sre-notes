# Part 1
## Chapter 1
- Systems don't run themselves, but how *should* they be run, particularly complex, large-scale ones?
- Historically software is run by system administrators who are in charge of assembling existing software components and deploying them to produce a fully-fledged system
- Because sysadmins require a vastly different skillset from developers, organisations are typically split into different teams; "development" and "operations"
- The biggest pitfall of sysadmin work is the direct cost of having a team that relies on manual intervention
- Deploying changes or handling events becomes costly as systems grow in size
- Another pitfall is the indirect cost of having wholly different teams
- Operations teams and sysadmins can have a very different view of how the system should be developed, operated, deployed, and maintained from that of developers
- Developers want to release quickly and see users adopt new features, operations teams want to make sure the service doesn't break and the lights stay on => the two are inherently in tension
- SRE teams focus on employing software engineers to run products and systems
- *SRE is what happens when you ask a software engineer to design an operations team*
- At Google SRE teams have about 50-60% standard software engineers and the other 40-50% are software engineers who also have technical skills related to operations and systems that are more rare for typical software engineers e.g. UNIX internals or networking
- SRE teams have the aptitude and skills to replace with automation what has historically been done manually by sysadmins
- To avoid overloading ops and ending up in a situation where more people are needed to do the same work over and over again, Google employs a 50% hard cap on all "ops work" for SREs - meaning service tickets, on-call, manual changes, etc.
- SREs should be mainly employed in engineering and development and systems need to be *automatic, not just automated*
- because SRE teams are directly modifying the code that they need to run, they are prone to rapid innovation and large acceptance of change
- SREs cross-train the entire group by means of exposing other developers to operations-concerns
- Reliability is thought of in terms of various considerations
	- what level of availability are users happy with?
	- what alternatives are to users who are dissatisfied with product's availability?
	- what happens to users' usage of the product at different availability levels
- Reliability hence isn't entirely about ensuring 24/7/365 availability but weighing that availability against new feature rollouts or features that can be measured as an *error budget* that the team can use to predictably take services down or lower availability to roll new features
	- Errors become a predicted, expected, needed, and a measured part of the process
- Monitoring is used to determine and measure availability
- Instead of constructing value/feature-based monitors, they can be split into three parts
	- Alerts that signify a critical failure and a human must take immediate action
	- Tickets that can be fixed later
	- Logs that don't need to be looked at but can be used for diagnostic or forensic purposes
	
## Chapter 2 - Production environment at Google, from the viewpoint of an SRE

- Goes pretty deep into the nuts and bolts of Google hardware architecture, doesn't necessarily provide much useful info unless you really wanna geek out