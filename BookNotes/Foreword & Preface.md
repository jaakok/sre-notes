# Foreword & Preface

- Google's transition into an IT-centered business
- DevOps evangelism in the wider tech community
- SRE was born out of a need to have more flexible system administration practices, born from an era where sysadmin work was highly manual labour
- SRE book contains personal individual accounts written as essays that talk about the journey of various practices, systems, and people in Google
- The book focuses on documenting the reasoning behind various infrastructure-level decisions, not so much anything to do with code or solving problems, but how a particular type of thinking resulted in solutions
- Majority of software engineering focuses on the initial birth of a software product. Software in production is seen as "stabilised" and needing little to no care. 
- This then brings into light the need for a higher level discipline, one that oversees the whole lifecycle of software products from its inception, deployment and operation, refinement and bug fixes, and eventual decommisioing
- This discipline uses and needs a wide range of skills, but has very separate concerns and focuses from other engineers
- The answer in Google's terms is SRE, or, Site Reliability Engineering

- **What exactly is Site Reliability Engineering?**
- First and foremost SRE is engineering
- SREs apply the principles of computer science and engineering to design and develop computing *systems*; typically large and distributed ones
- SRE tasks vary from time to time, sometimes it's the hands-on development of software products, sometimes it's building system infrastructure, backups, load balancing, and sometimes it's figuring out how to apply existing solutions to new systems
- Secondly SRE is about *reliability*
- A system isn't very useful if no one can use it
- SREs focus on improving the design and operation of systems to make them more scalable, reliable, and efficient
- Finally SRE is about operating *services*, usually built upon distributed computing systems, whether planet-scale storage, email for hundreds of millions of users, or Google's origin, web search
- the "site" in SRE originally referred to keeping google.com up and running